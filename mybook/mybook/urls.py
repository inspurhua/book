"""mybook URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from api.views import *
from api.util import handle_400, handle_401, handle_403, handle_404, handle_500

handler400 = handle_400
handler401 = handle_401
handler403 = handle_403
handler404 = handle_404
handler500 = handle_500

urlpatterns = [
    path('admin', admin.site.urls),
    path('tags/<int:id>', TagView.as_view(), name='tags_id'),
    path('tags', TagView.as_view(), name='tags'),

]
