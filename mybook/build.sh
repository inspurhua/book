#! /bin/bash
# 变量区==========================
whoami=zhanghua02
target=aczhanghua
# target=gdc.docker.iec.io
project=mybook
pythonenv=../../env2/bin/activate
push=no #yes or no
# 变量区==========================
# 代码加注释
# sed -i "1s/^/# Author: $whoami@inspur.com\n# Date: $(date +'%Y\/%m\/%d')\n\n/" $project/*.py
# sed -i "1s/^/# Author: $whoami@inspur.com\n# Date: $(date +'%Y\/%m\/%d')\n\n/" api/*.py
# 打包requirements.txt
source $pythonenv
pip freeze > ./requirements.txt
# 处理Dockfile
sed "s/__PROJECT__/$project/g" ./template/Dockerfile > ./Dockerfile
# 处理docker-compose.yml
tag=$target/my-server:$whoami-$(date +'%Y%m%d')
sed "s#gidc.docker.iec.io/my-server:wubai-20201117#$tag#g" ./template/docker-compose.yml > ./docker-compose.yml
docker build -t  $tag .


# push
if [ $push == yes ];
then
docker push $tag
fi