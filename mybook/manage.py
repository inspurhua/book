#!/usr/bin/env python
"""Django's command-line utility for administrative tasks."""
import os
import sys
from django.core.management.commands.migrate import Command
from django.core.cache import cache

def with_lock(func):
        def inner(*args,**kwargs):
            with cache.lock("migrate"):    
                """被装饰函数前需要添加的内容"""                 
                ret=func(*args,**kwargs) #被装饰函数        
                """被装饰函数后需要添加的内容"""        
                return ret   
        return inner
        
Command.handle = with_lock(Command.handle)

def main():
    """Run administrative tasks."""
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'mybook.settings')
    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc
    execute_from_command_line(sys.argv)


if __name__ == '__main__':
    main()
