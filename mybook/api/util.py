from django.http import JsonResponse
from django_redis import get_redis_connection

def handle_400(request, exception):
    return JsonResponse({'code': 400, 'msg': exception.__str__(), 'data': None}, status=400)


def handle_401(request, exception):
    return JsonResponse({'code': 401, 'msg': exception.__str__(), 'data': None}, status=401)


def handle_403(request, exception):
    return JsonResponse({'code': 403, 'msg': exception.__str__(), 'data': None}, status=403)


def handle_404(request, exception):
    return JsonResponse({'code': 404, 'msg': exception.__str__(), 'data': None}, status=404)


def handle_500(request):
    return JsonResponse({'code': 500, 'msg': '找不到', 'data': None}, status=500)

