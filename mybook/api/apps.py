from django.apps import AppConfig
from django.core.cache import cache


class ApiConfig(AppConfig):
    name = 'api'

    def ready(self):
        # 幂等操作,必须等迁移完数据库再干这个
        with cache.lock("app_ready"):
            try:
                from django.contrib.auth.models import User
                from api.models import Tag
                user, created = User.objects.get_or_create(username='zhanghua')
                if created:
                    user.email =  "zhanghua02@inspur.com"
                    user.password = '123456'
                    user.is_staff = True 
                    user.is_superUser = True
                    user.save()
                tags ="a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z".split(',')
                for tag_name in tags:
                    tag, created = Tag.objects.get_or_create(name=tag_name)
                    if created:
                        tag.save()
                        
                print("ready~~~~~~~~~~~")
            except:
                print("没有走啊，哈哈哈哈~~~~~~~~~~~")
