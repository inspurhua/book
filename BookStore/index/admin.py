from django.contrib import admin

# Register your models here.

from .models import *

admin.site.register(User)
admin.site.register(Article)
admin.site.register(SaleOrder)
admin.site.register(SaleOrderDetail)
