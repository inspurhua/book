from django.apps import AppConfig


class IndexConfig(AppConfig):
    name = 'index'

    def ready(self):
        # 幂等操作
        from django.contrib.auth.models import User
 
        if not User.objects.filter(username__exact="zhanghua").exists():
            User.objects.create_user("zhanghua", "zhanghua02@inspur.com", "123456", **{
                "is_staff": True,
                "is_superuser": False,
            })
        # Book.objects.filter(title__exact="Tornado").delete()
        print("Hello~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
