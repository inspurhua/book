from django.db import models
from django.utils.translation import gettext_lazy as _
# Create your models here.
class Article(models.Model):


    title = models.CharField("Title", max_length=50)
    content = models.TextField("Content")
    status = models.IntegerField("Status")

    create_at = models.DateTimeField("Create At", auto_now=False, auto_now_add=True)
    update_at = models.DateTimeField("Update At", auto_now=True, auto_now_add=False)


    class Meta:
        verbose_name = "Aritcle"
        verbose_name_plural = "Aritcles"

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("Aritcle_detail", kwargs={"pk": self.pk})

class User(models.Model):
    uname = models.CharField(max_length=50)
    upwd = models.CharField(max_length=100)
    #active inactive
    status = models.CharField(max_length=10)

    class Meta:
        verbose_name = "User"
        verbose_name_plural = "Users"

    def __str__(self):
        return self.uname

    def get_absolute_url(self):
        return reverse("User_detail", kwargs={"pk": self.pk})

class SaleOrder(models.Model):
    name = models.CharField(_("Order Name"), max_length=50)
    

    class Meta:
        verbose_name = _("SaleOrder")
        verbose_name_plural = _("SaleOrders")

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("SaleOrder_detail", kwargs={"pk": self.pk})
class SaleOrderDetail(models.Model):
    name = models.CharField(_("Order Detail Name"), max_length=50)
    parent = models.ForeignKey("SaleOrder", verbose_name=_("Parent ID"), on_delete=models.DO_NOTHING)
    qty = models.DecimalField(_("Qty"), max_digits=5, decimal_places=2)
    class Meta:
        verbose_name = _("SaleOrderDetail")
        verbose_name_plural = _("SaleOrderDetails")

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("SaleOrderDetail_detail", kwargs={"pk": self.pk})


